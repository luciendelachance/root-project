import { NgModule } from '@angular/core';
import { FeatureEchoHomepageComponent } from './feature-echo-homepage.component';
import { FeatureEchoHomepageRoutingModule } from './feature-echo-homepage-routing module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';

import { CommonModule } from '@angular/common';

import { EchoBoxComponent } from './components/echo-box/echo-box.component';

import { EchoMessageModule } from '@root-project/echo-message';

@NgModule({
  declarations: [
    FeatureEchoHomepageComponent,
    EchoBoxComponent,
  ],
  providers: [],
  imports: [
    CommonModule,
    FeatureEchoHomepageRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MatFormFieldModule, 
    MatInputModule,
    MatCardModule,
    EchoMessageModule,
  ]  
})
export class FeatureEchoHomepageModule {

}