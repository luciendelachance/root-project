import { Component } from '@angular/core';

@Component({
  selector: 'echo-homepage',
  templateUrl: './feature-echo-homepage.component.html',
  styleUrl: './feature-echo-homepage.component.css',
})
export class FeatureEchoHomepageComponent {}
