import { Action, createAction, props } from '@ngrx/store';

export enum ActionTypes {
    GetEchoMessage = 'Get Echo Message',
    GetEchoMessageSuccess = 'Get Echo Message success',
    GetEchoMessageError = 'Get Echo Message error',
    GetEchoMessageClear = 'Get Echo Message error',
}

export const getEchoMessage = createAction(ActionTypes.GetEchoMessage, props<{ message: string }>());
export const getEchoMessageSuccess = createAction(ActionTypes.GetEchoMessageSuccess, props<{ message: string }>());
export const getEchoMessageError = createAction(ActionTypes.GetEchoMessageError);
export const getEchoMessageClear = createAction(ActionTypes.GetEchoMessageClear);
