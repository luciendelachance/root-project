import { Injectable } from "@angular/core";
import { EchoMessageService } from "./echo-message.service";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { getEchoMessage, getEchoMessageError, getEchoMessageSuccess } from "./echo-message.actions";
import { catchError, map, mergeMap, of } from "rxjs";

@Injectable()
export class EchoMessageEffects {

    constructor(
        private actions$: Actions,
        private echoMessageService: EchoMessageService
    ) {
    }

    sendEchoMessage$ = createEffect(() => 
        this.actions$.pipe(
            ofType(getEchoMessage),
            mergeMap((payload) => {
                return this.echoMessageService.call(payload.message).pipe(
                    map(payload => getEchoMessageSuccess({ message: payload })),
                    catchError(_ => of(getEchoMessageError()))
                );
            })
        )
    );
}