import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";

import { RouterModule, Routes } from "@angular/router";
import { FeatureEchoHomepageModule } from '@root-project/feature-echo-homepage';

export const APP_ROUTES: Routes = [
    {
        path: 'home',
        loadChildren: () => import('@root-project/feature-echo-homepage').then(m => m.FeatureEchoHomepageModule),
    },
    {
        path: '', redirectTo: 'home', pathMatch: 'full'
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(APP_ROUTES),
        FeatureEchoHomepageModule
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {

}