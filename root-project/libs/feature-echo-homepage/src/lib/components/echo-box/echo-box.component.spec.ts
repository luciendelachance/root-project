import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EchoBoxComponent } from './echo-box.component';

describe('EchoBoxComponent', () => {
  let component: EchoBoxComponent;
  let fixture: ComponentFixture<EchoBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EchoBoxComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(EchoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
