import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FeatureEchoHomepageComponent } from './feature-echo-homepage.component';

describe('FeatureEchoHomepageComponent', () => {
  let component: FeatureEchoHomepageComponent;
  let fixture: ComponentFixture<FeatureEchoHomepageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureEchoHomepageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(FeatureEchoHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
