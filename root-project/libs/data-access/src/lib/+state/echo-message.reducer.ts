import { Action, createReducer, on } from "@ngrx/store";
import { getEchoMessage, getEchoMessageClear, getEchoMessageError, getEchoMessageSuccess } from "./echo-message.actions";

export interface EchoMessageState {
    message: string;
    isPending: boolean;
    isError: boolean;
}

export const initialState: EchoMessageState = {
    message: '',
    isPending: true,
    isError: false,
};

export const FEATURE_KEY = 'echo-message';

const echoMessageReducer = createReducer(
    initialState, 
    on(getEchoMessage, (state, _) => ({...state, isPending: true })),
    on(getEchoMessageSuccess, (_, action) => {
        console.log(action);
        return { message: action.message, isPending: false, isError: false };
    }),
    on(getEchoMessageError, (state, _) => ({...state, isPending: false, isError: false })),
    on(getEchoMessageClear, () => ({message: '', isPending: false, isError: false })),
)
 
export function reducer(state = initialState, action: Action) {
    return echoMessageReducer(state, action);
}