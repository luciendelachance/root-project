import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { debounceTime } from 'rxjs';

import { getEchoMessage } from '@root-project/echo-message';
import { selectEchoMessage } from '@root-project/echo-message';

@Component({
  selector: 'echo-box',
  templateUrl: './echo-box.component.html',
  styleUrl: './echo-box.component.css',
})
export class EchoBoxComponent implements OnInit {

  echoMessage: string = '';
  echoFormControl = new FormControl('', [Validators.required]);

  constructor(private store: Store) {    
  }

  ngOnInit(): void {
    this.echoFormControl.valueChanges.pipe(
      debounceTime(2000),
    ).subscribe(echoMessage => 
      this.store.dispatch(getEchoMessage({message: echoMessage ?? '' }))
    );

    this.store.select(selectEchoMessage).subscribe(echoMessageFromRemote => {
      this.echoMessage = echoMessageFromRemote;
      console.log(echoMessageFromRemote);
    })
  }

  
}
