import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";

import { FeatureEchoHomepageModule } from '@root-project/feature-echo-homepage';
import { AppRoutingModule } from "./app-routing.module";

import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

@NgModule({
    declarations: [AppComponent],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        FeatureEchoHomepageModule,
        MatToolbarModule, 
        MatButtonModule, 
        MatIconModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
    ],
    bootstrap: [AppComponent]
})
export class AppModule {

}