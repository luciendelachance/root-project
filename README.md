# Root Project

A simple empty project setup that can be used to build a monorepo application suite.

## How to 

Use either: 

- `echo-backend` 
- `echo-frontend`

### Serve (for local testing)
- `nx run echo-backend:serve`

### Test
- `nx run echo-backend:test`

### Build
- `nx run echo-backend:build`


## TODO:
- Add a backend project with a GET rest endpoint that reverses the input string (e.g. /api/echo/foobar -> raboof) as a NX module.

## Technologies:

### CI/CD and build:
- Gitlab
- NX
- Maven

### Backend
- Java 21
- Spring Boot 

### Frontend
- Angular
- NGRX