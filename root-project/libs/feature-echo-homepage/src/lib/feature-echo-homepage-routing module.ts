import { NgModule } from '@angular/core';
import { FeatureEchoHomepageComponent } from './feature-echo-homepage.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: FeatureEchoHomepageComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureEchoHomepageRoutingModule {

}