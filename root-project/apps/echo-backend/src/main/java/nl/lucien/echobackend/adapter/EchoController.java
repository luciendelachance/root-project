package nl.lucien.echobackend.adapter;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
public class EchoController {
    
	@GetMapping("/echo/{input}")
    @CrossOrigin("http://localhost:4200")
	public ResponseEntity<String> greeting(@NonNull @PathVariable(value = "input") String input) {
        String reversedInput = reverse(input);
		return new ResponseEntity<String>(reversedInput, HttpStatus.OK);
	}

    private String reverse(String input) {
        byte[] strAsByteArray = input.getBytes();
 
        byte[] result = new byte[strAsByteArray.length];
 
        // Store result in reverse order into the
        // result byte[]
        for (int i = 0; i < strAsByteArray.length; i++) {
            result[i] = strAsByteArray[strAsByteArray.length - i - 1];
        }

        return new String(result);
    }
}