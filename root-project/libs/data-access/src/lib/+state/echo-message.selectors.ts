import { createFeatureSelector, createSelector } from "@ngrx/store";
import { EchoMessageState, FEATURE_KEY } from "./echo-message.reducer";

export const selectEchoMessageState = createFeatureSelector<EchoMessageState>(FEATURE_KEY);
export const selectEchoMessage = createSelector(selectEchoMessageState, state => state.message);