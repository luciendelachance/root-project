import { HttpClient } from '@angular/common/http';

import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class EchoMessageService {

    constructor(private readonly http: HttpClient) {}
    
    call(message: string): Observable<string> {
        // TODO: do a proper url generation. Not static localhost.
        const url = `http://localhost:8081/echo/${message}`;
        return this.http.get<string>(url);
    }
}